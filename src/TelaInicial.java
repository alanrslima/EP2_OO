import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JFrame;


public class TelaInicial extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final int WIDTH_TELA_PADRAO = 600;
	public static final int HEIGHT_TELA_PADRAO = 620;
	
	private static final int TELA_JOGADOR = 1;
	private static final int TELA_INSTRUCOES = 2;
	
	TelaJogador telaJogador = new TelaJogador(this);
	TelaInstrucoes telaInst = new TelaInstrucoes(this);
	
	public static void main(String[] args) {
		new TelaInicial();
	
	}
	
	public TelaInicial() {
		criaTela();
	}
	
	private void proximaTela(int tela) {
		if (tela == TELA_INSTRUCOES) {
			telaInst.iniciaTela();
		}else if (tela == TELA_JOGADOR) {
			telaJogador.iniciaTela();
		}
		this.setVisible(false);
	}
	
	private void criaTela() {
		
		desenhaTela Fundo = new desenhaTela();
		this.setTitle("Batalha Naval 2.0");
		this.setSize(WIDTH_TELA_PADRAO, HEIGHT_TELA_PADRAO);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);
		this.getContentPane().add("Center", Fundo);
		
		this.setVisible(true);	
		Fundo.paint(Fundo.getGraphics());
		
		Fundo.addMouseListener(new MouseListener() {
				@Override
			public void mouseReleased(MouseEvent e) {
				 int x=e.getX();
			     int y=e.getY();		
			     
			     if ((x > 195 && x < 405) && (y > 410 && y < 465)) {
			    	 proximaTela(TELA_JOGADOR);
			     }
			     if ((x > 195 && x < 405) && (y > 485 && y < 540)) {
			    	 proximaTela(TELA_INSTRUCOES);
			     }
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {				
			}
		});

	}
	
	private class desenhaTela extends Canvas {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
		public void paint(Graphics g) {
			
			final Image imgFundo = new ImageIcon("imagens/Telas/TelaInicial.jpg").getImage(); 
			g.drawImage(imgFundo, 0, 0,null);
			
		}
	}
}
