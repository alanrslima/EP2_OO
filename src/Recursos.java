
public class Recursos {

	private int saldo;
	
	public Recursos(int saldoInicial) {
		this.saldo = saldoInicial;
	}
	
	public boolean possuiSaldo(int debito) {
		if(debito <= saldo) {
			return true;
		}else{
			return false;
		}
	}
	
	public boolean debitaSaldo(int debito) {
		if(debito <= saldo) {
			saldo = saldo - debito;
			return true;
		}else {
			return false;
		}
	}
	
	public int getSaldo() {
		return this.saldo;
	}
	
	public boolean setSaldo(int saldo) {
		if(saldo >= 0)
		{
			this.saldo = saldo;
			return true;
		}
		return false;
	}
}
