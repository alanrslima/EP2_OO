import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class TelaJogador extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final int TELA_NIVEL = 1;
	private static final int TELA_INICIAL = 2;
	
	private TelaInicial telaIni;
	
	private desenhaTela Fundo = new desenhaTela();
	private JTextField txtNome = new JTextField();
	private int avatarSelecionado = 1;
	public Jogador player = new Jogador();
	
	private final Image imgAvatar1 = new ImageIcon("imagens/Avatares/avatar1.png").getImage();
	private final Image imgAvatar2 = new ImageIcon("imagens/Avatares/avatar2.png").getImage();
	private final Image imgAvatar3 = new ImageIcon("imagens/Avatares/avatar3.png").getImage();
	private final Image imgAvatar4 = new ImageIcon("imagens/Avatares/avatar4.png").getImage();
	private final Image imgAvatar5 = new ImageIcon("imagens/Avatares/avatar5.png").getImage();

	public TelaJogador(TelaInicial telaAnterior) {
		criaTela();
		telaIni = telaAnterior;
	}
	
	public void iniciaTela() {
		this.setVisible(true);
		Fundo.paint(Fundo.getGraphics()); 
	}
	
	private void proximaTela(int tela) {
		if (tela == TELA_INICIAL) {
			telaIni.setVisible(true);
		}else if (tela == TELA_NIVEL) {
			TelaDificuldade telaDif = new TelaDificuldade();
			telaDif.iniciaTela(this);
		}
		this.setVisible(false);
	}
	
	private void janelaMensagem(String msg) {
		JOptionPane.showMessageDialog(this, msg, "Atenção marinheiro", JOptionPane.ERROR_MESSAGE);
	}
	
	private boolean jogadorCadastrado() {

		if (txtNome.getText().trim().equals("")) {
			return false;
		}
		player.setNome(txtNome.getText());
		if (avatarSelecionado == 1) {
			player.setFoto(imgAvatar1);
		}else if (avatarSelecionado == 2) {
			player.setFoto(imgAvatar2);
		}else if(avatarSelecionado == 3) {
			player.setFoto(imgAvatar3);
		}else if(avatarSelecionado == 4) {
			player.setFoto(imgAvatar4);
		}else if(avatarSelecionado == 5) {
			player.setFoto(imgAvatar5);
		}
		return true;
	}
	
	private void criaTela() {
		this.setTitle("Jogador");
		this.setSize(TelaInicial.WIDTH_TELA_PADRAO, TelaInicial.HEIGHT_TELA_PADRAO);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);
		this.add(txtNome);
		this.getContentPane().add("Center", Fundo);
		
		txtNome.setSize(300,30);
		txtNome.setLocation(250,70);
		
		Fundo.addMouseListener(new MouseListener() {
				@Override
			public void mouseReleased(MouseEvent e) {
				 int x=e.getX();
			     int y=e.getY();		
			     
			     //Selecionando avatar
			     if (y > 160 && y < 205) {
			    	 if (x > 250 && x < 295) {
			    		 avatarSelecionado = 1;
			    	 }else if(x > 296 && x < 340){
			    		 avatarSelecionado = 2;
			    	 }else if(x > 341 && x < 385){
			    		 avatarSelecionado = 3;
			    	 }else if(x > 386 && x < 430){
			    		 avatarSelecionado = 4;
			    	 }else if(x > 431 && x < 475){
			    		 avatarSelecionado = 5;
			    	 }
			    	 Fundo.paint(Fundo.getGraphics());
			     }
			     
			     //Botoes
			     if (y > 525 && y < 585) {
			    	 if (x > 17 && x < 235) {
			    		 proximaTela(TELA_INICIAL);
			    	 }else if(x > 370 && x < 585) {
			    		 if (jogadorCadastrado()) {
			    			 proximaTela(TELA_NIVEL);
			    		 }else {
			    			 janelaMensagem("Preencha seu nome!");
			    		 }
			    	 }
			     }
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {				
			}
		});
	}
	
	private class desenhaTela extends Canvas {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
		public void paint(Graphics g) {
			
			final Image imgFundo = new ImageIcon("imagens/Telas/TelaJogador.jpg").getImage(); 
			final Image imgSelecionada = new ImageIcon("imagens/Actions/selecionaAvatar.png").getImage();
			g.drawImage(imgFundo, 0, 0,null);
			
			if (avatarSelecionado == 1){
				g.drawImage(imgSelecionada, 235, 150, null);
				g.drawImage(imgAvatar1, 34, 53, null);
			}else if(avatarSelecionado == 2) {
				g.drawImage(imgSelecionada, 282, 150, null);
				g.drawImage(imgAvatar2, 34, 53, null);
			}else if (avatarSelecionado == 3) {
				g.drawImage(imgSelecionada, 330, 150, null);
				g.drawImage(imgAvatar3, 34, 53, null);
			}else if(avatarSelecionado == 4) {
				g.drawImage(imgSelecionada, 375, 150, null);
				g.drawImage(imgAvatar4, 34, 53, null);
			}else if (avatarSelecionado == 5) {
				g.drawImage(imgSelecionada, 420, 150, null);
				g.drawImage(imgAvatar5, 34, 53, null);
			}
		}
	}
}
