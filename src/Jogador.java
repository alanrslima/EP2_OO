import java.awt.Image;

public class Jogador {

	private String nome;
	private Image foto;
	private int pontuacao;
	private Image bandeira;
	
	public Jogador(String nome, String sexo, Image foto, int pontuacao, Image bandeira) {
		this.nome = nome;
		this.foto = foto;
		this.pontuacao = pontuacao;
		this.bandeira = bandeira;
	}
	
	public Jogador() {
		this.nome = "";
		this.foto = null;
		this.pontuacao = 0;
		this.bandeira = null;
	}
	
	public Image getBandeira() {
		return this.bandeira;
	}
	public int getPontuacao() {
		return this.pontuacao;
	}
	public String getNome() {
		return this.nome;
	}
	public Image getFoto() {
		return this.foto;
	}
	public void setPontuacao(int pontuacao) {
		this.pontuacao = pontuacao;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setFoto(Image foto) {
		this.foto = foto;
	}
	public void setBandeira(Image bandeira) {
		this.bandeira = bandeira;
	}
}
