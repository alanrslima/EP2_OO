
public class CanvasThread extends Thread {
	private TelaBatalhaNaval telaCanvas;
	private boolean running = false; 
	
	public CanvasThread(TelaBatalhaNaval tela) {
		this.telaCanvas = tela;
	}
	
	@Override
	public void run() {
		while(running) {	
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			telaCanvas.Fundo.paint(telaCanvas.Fundo.getGraphics());
		}
	}
	
	public boolean getRunning() {
		return this.running;
	}
	public void setRunning(boolean running) {
		this.running = running;
	}
}
