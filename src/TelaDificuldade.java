import java.awt.Canvas;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.ImageIcon;
import javax.swing.JFrame;

public class TelaDificuldade extends JFrame{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final int INICIANTE = 1;
	private static final int AMADOR = 2;
	private static final int PROFISSIONAL = 3;
	private desenhaTela Fundo = new desenhaTela();
	
	private	TelaJogador telaJogador;
	private TelaBatalhaNaval telaBatalha;

	public Jogador player;
	public ReadTxt map;

	public TelaDificuldade() {
		criaTela();	
	}
	
	public void iniciaTela(TelaJogador telaAnterior) {
		telaJogador = telaAnterior;
		player = telaJogador.player;
		this.setVisible(true);
		Fundo.paint(Fundo.getGraphics());
	}
	
	private void voltaTela() {
		telaJogador.setVisible(true);
		this.setVisible(false);
	}
	
	private void startJogo(int nivelJogo) {
		if (nivelJogo == INICIANTE) {
			map = new ReadTxt(3);
		}else if(nivelJogo == AMADOR) {
			map = new ReadTxt(2); // 2 ou 5
		}else if(nivelJogo == PROFISSIONAL) {
			map = new ReadTxt(4); // 1 ou 4
		}
		this.setVisible(false);
		telaBatalha = new TelaBatalhaNaval();
		telaBatalha.criaTela(this);
	}
	
	private void criaTela() {

		this.setTitle("Dificuldades");
		this.setSize(TelaInicial.WIDTH_TELA_PADRAO, TelaInicial.HEIGHT_TELA_PADRAO);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setResizable(false);
		this.getContentPane().add("Center", Fundo);
		Fundo.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				 int x=e.getX();
			     int y=e.getY();		
			     
			     if ((x > 20 && x < 235) && (y > 525 && y < 580)) {
				    	voltaTela();
				 }
			     if ((x > 205 && x < 425) && (y > 130 && y < 185)) {
			    	 startJogo(INICIANTE);
				 }
			     if ((x > 205 && x < 425) && (y > 215 && y < 275)) {
			    	 startJogo(AMADOR);
				 }
			     if ((x > 205 && x < 425) && (y > 305 && y < 360)) {
			    	 startJogo(PROFISSIONAL);
				 }
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {				
			}
		});
	}
	
	private class desenhaTela extends Canvas {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
		public void paint(Graphics g) {
		
			final Image imgFundo = new ImageIcon("imagens/Telas/TelaNivel.jpg").getImage(); 
			g.drawImage(imgFundo, 0, 0,null);
		}
	}
}
