Batalha Naval 2.0

A Batalha Naval 2.0 é uma versão modificada, jogada por apenas um jogador (Single-Player), que tem como objetivo destruir, com os recursos disponíveis, todos os alvos inimigo em um campo de batalha.

O jogo possui os seguintes elementos:

Tabuleiro: Consiste em uma matriz, NxM, com coordenadas alfanuméricas (A1, C4, D2, etc) onde as embarcações serão dispostas. 

Embarcações: Barcos, navios, submarinos, entre outras embarcações que serão secretamente posicionadas no tabuleiro. O objetivo do jogador é destruir todas elas.

Recursos: Fundos disponíveis para o jogador, que permitem a utilização de habilidades.

Habilidades: Ações de ataque ou estratégia que servirão para o jogador atingir seu objetivo.

Inicialmente, as embarcações são posicionadas e escondidas no tabuleiro sem o conhecimento do jogador. Este por sua vez, deve elaborar uma estratégia, pois possui uma quantidade limitada de recursos disponíveis para atacar a esquadra inimiga. Estes recursos servem para possibilitar o uso das habilidades do jogador, que são:

Atacar uma posição do tabuleiro;
Atacar uma área 2x2 do tabuleiro;
Atacar uma linha/coluna completa do tabuleiro;

Cada uma destas habilidades possui um custo diferente, e deve ser utilizada com cautela, pois caso o jogador fique sem recursos antes de destruir todas as embarcações inimigas, perde o jogo.

Se o jogador destruir todas as embarcações inimigas antes que seus recursos acabem, vence o jogo.

A estrutura de telas do jogo consiste em um tela inicial, onde o jogador poderá acessar as instruções da Batalha Naval 2.0 ou acessar a opção de "Jogar". Neste módulo o usuário irá fornecer seu nome, escolher um avatar. 
Logo após será direcionado para uma tela de escolha de nível do jogo, sendo eles, "Iniciante, amador e intermediário". Esses níveis são baseados em arquivos .txt que contem  matrizes representativas das embarcações em formatos ja pré-definidos. Estes arquivos de texto devem estar na pasta "maps".
Após a escolha do mapa, o jogo terá início, onde o jogador deverá descobrir a posição de todas as embarcações para ganhar o jogo fazendo uso das habilidades disponíveis no canto direito da tela e que ja foram explicadas acima. No canto esquerdo estarão os dados do jogador e a sua quantidade de "coins". Caso essas moedas acabem antes de descobrir todas as embarcações, o jogador perde. Portanto utilize seus recursos com sabedoria e bom jogo!